﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

/// <author>Geert Ariën</author>
/// <date>21 april 2014</date>

namespace Doolhof {

    /// <summary>
    /// Een struct die de gegevens van een muur bevat
    /// </summary>
    struct Wall {

        /// <summary>
        /// Positie x1
        /// </summary>
        private int x1;

        /// <summary>
        /// Positie x2
        /// </summary>
        private int x2;

        /// <summary>
        /// Positie y1
        /// </summary>
        private int y1;

        /// <summary>
        /// Positie y2
        /// </summary>
        private int y2;

        /// <summary>
        /// De hoogte van de muur
        /// </summary>
        private double height;

        /// <summary>
        /// De breedte van de muur
        /// </summary>
        private double width;

        /// <summary>
        /// De positie in x
        /// </summary>
        private double positionX;

        /// <summary>
        /// De positie in y
        /// </summary>
        private double positionY;

        /// <summary>
        /// Het GeometryModel3D dat de muur grafish voorsteldt
        /// </summary>
        private GeometryModel3D wallModel;

        /// <summary>
        /// Readonly eigenschap voor positie x1
        /// </summary>
        public int X1 {
            get {
                return this.x1;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor positie x2
        /// </summary>
        public int X2 {
            get {
                return this.x2;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor positie y1
        /// </summary>
        public int Y1 {
            get {
                return this.y1;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor positie y2
        /// </summary>
        public int Y2 {
            get {
                return this.y2;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor de hoogte
        /// </summary>
        public double Height {
            get {
                return this.height;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor de breedte
        /// </summary>
        public double Width {
            get {
                return this.width;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor de positie in x
        /// </summary>
        public double PositionX {
            get {
                return this.positionX;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor de positie in y
        /// </summary>
        public double PositionY {
            get {
                return this.positionY;
            }
        }

        /// <summary>
        /// Readonly eigenschap voor het GeometryModel3D dat de muur voorsteld
        /// </summary>
        public GeometryModel3D WallModel {
            get {
                return this.wallModel;
            }
        }

        /// <summary>
        /// Niet-standaard constructor
        /// </summary>
        /// <param name="x1">int: positie x1</param>
        /// <param name="x2">int: positie x2</param>
        /// <param name="y1">int: positie y1</param>
        /// <param name="y2">int: positie y2</param>
        public Wall(int x1, int x2, int y1, int y2) {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;

            this.height = y2 - y1;
            this.width = x2 - x1;

            this.positionX = x1 + this.width / 2;
            this.positionY = y1 + this.height / 2;

            this.wallModel = new GeometryModel3D();
            MeshGeometry3D mesh = new MeshGeometry3D();
            Point3DCollection points = new Point3DCollection();
            points.Add(new Point3D(x1, y1, 1));
            points.Add(new Point3D(x2, y1, 1));
            points.Add(new Point3D(x1, y1, 2.5));
            points.Add(new Point3D(x2, y1, 2.5));
            points.Add(new Point3D(x1, y2, 1));
            points.Add(new Point3D(x2, y2, 1));
            points.Add(new Point3D(x1, y2, 2.5));
            points.Add(new Point3D(x2, y2, 2.5));
            mesh.Positions = points;
            mesh.TriangleIndices = new Int32Collection(new int[] { 0, 1, 2, 3, 2, 1, 1, 5, 3, 5, 7, 3, 7, 5, 4, 4, 6, 7, 6, 2, 3, 3, 7, 6, 0, 4, 1, 1, 4, 5, 0, 2, 4, 2, 6, 4 });
            this.wallModel.Geometry = mesh;
        }

    }
}
