﻿using System;
using System.Windows.Media.Media3D;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

/// <author>Geert Ariën</author>
/// <date>21 april 2014</date>

namespace Doolhof {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        /// <summary>
        /// De maximum allowed angle for turning the maze
        /// </summary>
        private const int MAX_ANGLE = 10;

        /// <summary>
        /// De gravitatie kracht in centimeters
        /// </summary>
        private const double GRAVITY_ACCELERATION = 981;

        /// <summary>
        /// De refresh rate voor de gameloop in ms
        /// </summary>
        private const int REFRESH_RATE = 10;

        /// <summary>
        /// De waarde van de rolling friction (wrijving bal met tafel)
        /// </summary>
        private const double ROLLING_FRICTION = 0.03;

        /// <summary>
        /// De afmeting van de tafel in centimeters
        /// </summary>
        private const double MEASUREMENT_TABLE = 30;

        /// <summary>
        /// De coëfficient van restitutie (botsingen)
        /// </summary>
        private const double RESTITUTION_COEFFICIENT = 0.4;

        /// <summary>
        /// De waarde van de refresh rate t.o.v van 1 seconde
        /// </summary>
        private readonly double TIMEFRAME;

        /// <summary>
        /// De afmeting van een eenheid in het assenstelsel (in centimeters)
        /// </summary>
        private readonly double CONVERSION_RATE;

        /// <summary>
        /// De rol weerstand bij een stilstaande bal
        /// </summary>
        private readonly double ROLLING_RESIST;

        /// <summary>
        /// De snelheid van de bal in de x-as
        /// </summary>
        private double speedX;

        /// <summary>
        /// De snelheid van de bal in de y-as
        /// </summary>
        private double speedY;

        /// <summary>
        /// De lijst met de muren van het doolhof
        /// </summary>
        private List<Wall> walls; 

        /// <summary>
        /// Initializeer de instellingen
        /// </summary>
        public MainWindow() {
            InitializeComponent();

            walls = new List<Wall>();

            // Add the corner walls
            modelGroupMain.Children.Add(AddWall(0, 46, 0, 1));
            modelGroupMain.Children.Add(AddWall(0, 1, 1, 45));
            modelGroupMain.Children.Add(AddWall(0, 46, 45, 46));
            modelGroupMain.Children.Add(AddWall(45, 46, 1, 45));

            // Add all other walls
            modelGroupMain.Children.Add(AddWall(1, 16, 6, 7));
            modelGroupMain.Children.Add(AddWall(21, 45, 6, 7));
            modelGroupMain.Children.Add(AddWall(3, 4, 7, 10));
            modelGroupMain.Children.Add(AddWall(9, 10, 7, 10));
            modelGroupMain.Children.Add(AddWall(15, 16, 7, 10));
            modelGroupMain.Children.Add(AddWall(21, 22, 7, 10));
            modelGroupMain.Children.Add(AddWall(32, 33, 7, 10));
            modelGroupMain.Children.Add(AddWall(39, 40, 7, 10));
            modelGroupMain.Children.Add(AddWall(27, 28, 7, 33));
            modelGroupMain.Children.Add(AddWall(15, 16, 12, 19));
            modelGroupMain.Children.Add(AddWall(21, 22, 12, 19));
            modelGroupMain.Children.Add(AddWall(33, 40, 12, 13));
            modelGroupMain.Children.Add(AddWall(42, 43, 12, 16));
            modelGroupMain.Children.Add(AddWall(33, 40, 15, 16));
            modelGroupMain.Children.Add(AddWall(3, 10, 15, 22));
            modelGroupMain.Children.Add(AddWall(15, 22, 21, 22));
            modelGroupMain.Children.Add(AddWall(18, 25, 24, 31));
            modelGroupMain.Children.Add(AddWall(33, 34, 16, 36));
            modelGroupMain.Children.Add(AddWall(34, 42, 18, 19));
            modelGroupMain.Children.Add(AddWall(42, 43, 18, 21));
            modelGroupMain.Children.Add(AddWall(39, 43, 21, 22));
            modelGroupMain.Children.Add(AddWall(39, 45, 24, 25));
            modelGroupMain.Children.Add(AddWall(39, 45, 27, 28));
            modelGroupMain.Children.Add(AddWall(39, 45, 30, 31));
            modelGroupMain.Children.Add(AddWall(39, 40, 31, 34));
            modelGroupMain.Children.Add(AddWall(1, 16, 27, 28));
            modelGroupMain.Children.Add(AddWall(15, 16, 28, 34));
            modelGroupMain.Children.Add(AddWall(6, 13, 30, 31));
            modelGroupMain.Children.Add(AddWall(6, 7, 33, 40));
            modelGroupMain.Children.Add(AddWall(7, 16, 39, 40));
            modelGroupMain.Children.Add(AddWall(1, 16, 42, 43));
            modelGroupMain.Children.Add(AddWall(21, 28, 33, 34));
            modelGroupMain.Children.Add(AddWall(21, 22, 34, 43));
            modelGroupMain.Children.Add(AddWall(13, 21, 36, 37));
            modelGroupMain.Children.Add(AddWall(12, 13, 33, 37));
            modelGroupMain.Children.Add(AddWall(22, 36, 42, 43));
            modelGroupMain.Children.Add(AddWall(36, 37, 40, 43));
            modelGroupMain.Children.Add(AddWall(27, 37, 39, 40));
            modelGroupMain.Children.Add(AddWall(27, 42, 36, 37));
            modelGroupMain.Children.Add(AddWall(42, 43, 33, 45));

            speedX = 0;
            speedY = 0;
            TIMEFRAME = REFRESH_RATE / 1000.0;
            CONVERSION_RATE = MEASUREMENT_TABLE / 46;
            ROLLING_RESIST = ROLLING_FRICTION / ((this.Sphere.Bounds.SizeX / 2) * CONVERSION_RATE);
        }

        /// <summary>
        /// Start de gameloop van zodra het venster geladen is
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, REFRESH_RATE);
            timer.Start();
            
        }

        /// <summary>
        /// Uitvoering van de gameloop
        /// </summary>
        void timer_Tick(object sender, EventArgs e) {
            double offsetX = 0.0;
            double offsetY = 0.0;

            CalculateMovement(ref offsetX, ref offsetY);
            DetectColission(ref offsetX, ref offsetY);

            movementSphere.OffsetY += offsetY;
            movementSphere.OffsetX += offsetX;

            DetectInput();
        }

        /// <summary>
        /// Detecteren van input via het keyboard
        /// </summary>
        private void DetectInput() {
            if (Keyboard.IsKeyDown(Key.Right)) {
                if (YAngleRotation.Angle < MAX_ANGLE) {
                    YAngleRotation.Angle += 0.5;
                }
            }
            if (Keyboard.IsKeyDown(Key.Left)) {
                if (YAngleRotation.Angle > -MAX_ANGLE) {
                    YAngleRotation.Angle -= 0.5;
                }
            }
            if (Keyboard.IsKeyDown(Key.Down)) {
                if (XAngleRotation.Angle < MAX_ANGLE) {
                    XAngleRotation.Angle += 0.5;
                }
            }
            if (Keyboard.IsKeyDown(Key.Up)) {
                if (XAngleRotation.Angle > -MAX_ANGLE) {
                    XAngleRotation.Angle -= 0.5;
                }
            }
        }

        /// <summary>
        /// Berekenen van de verplaatsing van de bal
        /// </summary>
        /// <param name="offsetX">ref double: de verplaatsing in X</param>
        /// <param name="offsetY">ref double: de verplaatsing in Y</param>
        private void CalculateMovement(ref double offsetX, ref double offsetY) {

            // als de bal niet in beweging is heeft de rolweerstand een andere waarde
            double rollingFriction = speedY == 0 && speedX == 0 ? ROLLING_RESIST : ROLLING_FRICTION;

            // teken van acceleratie wordt bepaald door de sinus
            double acceleration = Math.Sin(XAngleRotation.Angle * Math.PI / 180) * GRAVITY_ACCELERATION * (5.0 / 7.0) * -1;
            // teken van de frictie wordt gelijk gesteld aan het teken van de snelheid, deze krachten zijn tegengesteld dus worden later van elkaar afgetrokken
            // als de snelheid nul is de frictie tegengesteld aan de acceleratie
            int signFriction = speedY != 0 ? Math.Sign(speedY) : Math.Sign(acceleration);
            // de frictie wordt berekened aan de hand van de cosinus
            double friction = Math.Cos(XAngleRotation.Angle * Math.PI / 180) * GRAVITY_ACCELERATION * rollingFriction * signFriction;
            double newSpeedY = speedY + (acceleration - friction) * TIMEFRAME;

            // als de frictie groter is dan de acceleratie kan de acceleratie niet van richting veranderen en wordt de snelheid 0
            if (Math.Abs(friction) > Math.Abs(acceleration) && Math.Sign(newSpeedY) != Math.Sign(speedY) && speedY != 0) {
                double limitedTimeframe = Math.Abs(speedY / (acceleration - friction));
                offsetY = CONVERSION_RATE * (speedY * limitedTimeframe + (acceleration - friction) * Math.Pow(limitedTimeframe, 2) / 2);
                speedY = 0;
            }
            // als de acceleratie groter of gelijk is aan de frictie vindt de normale acceleratie plaats
            else if (Math.Abs(friction) <= Math.Abs(acceleration) || speedY != 0) {
                offsetY = CONVERSION_RATE * (speedY * TIMEFRAME + (acceleration - friction) * Math.Pow(TIMEFRAME, 2) / 2);
                speedY = newSpeedY;
            }

            // Dezelfde berekening maar dan voor de x-as
            acceleration = Math.Sin(YAngleRotation.Angle * Math.PI / 180) * GRAVITY_ACCELERATION * (5.0 / 7.0);
            signFriction = speedX != 0 ? Math.Sign(speedX) : Math.Sign(acceleration);
            friction = Math.Cos(YAngleRotation.Angle * Math.PI / 180) * GRAVITY_ACCELERATION * rollingFriction * signFriction;
            double newSpeedX = speedX + (acceleration - friction) * TIMEFRAME;

            if (Math.Abs(friction) > Math.Abs(acceleration) && Math.Sign(newSpeedX) != Math.Sign(speedX) && speedX != 0) {
                double limitedTimeframe = Math.Abs(speedX / (acceleration - friction));
                offsetX = CONVERSION_RATE * (speedX * limitedTimeframe + (acceleration - friction) * Math.Pow(limitedTimeframe, 2) / 2);
                speedX = 0;
            }
            else if (Math.Abs(friction) <= Math.Abs(acceleration) || speedX != 0) {
                offsetX = CONVERSION_RATE * (speedX * TIMEFRAME + (acceleration - friction) * Math.Pow(TIMEFRAME, 2) / 2);
                speedX = newSpeedX;
            }
        }

        /// <summary>
        /// Detecteren botsing van de bal met de muren van de doolhof en de verplaatsing aanpassen indien dit nodig is
        /// </summary>
        /// <param name="offsetX">ref double: de verplaatsing in X</param>
        /// <param name="offsetY">ref double: de verplaatsing in Y</param>
        private void DetectColission(ref double offsetX, ref double offsetY) {
            double circleRadius = this.Sphere.Bounds.SizeX / 2;
            double circlePositionX = movementSphere.OffsetX + offsetX;
            double circlePositionY = movementSphere.OffsetY + offsetY;

            foreach (Wall wall in walls) {
                double circleDistanceX = Math.Abs(circlePositionX - wall.PositionX);
                double circleDistanceY = Math.Abs(circlePositionY - wall.PositionY);

                if (circleDistanceX > (wall.Width / 2 + circleRadius) || circleDistanceY > (wall.Height / 2 + circleRadius)) {
                    // de bal botst niet met de muur
                    continue;
                }
                else if (circleDistanceX <= (wall.Width / 2)) {
                    // de bal botst met de muur in de y-as
                    double distanceWall = Math.Abs(movementSphere.OffsetY - wall.PositionY) - circleRadius - wall.Height / 2;
                    double bounceBack = Math.Abs(circlePositionY - movementSphere.OffsetY) - distanceWall;
                    int relativePosition = movementSphere.OffsetY > wall.PositionY ? 1 : -1;
                    offsetY = relativePosition * (bounceBack - distanceWall) * RESTITUTION_COEFFICIENT;
                    this.speedY *= -1 * RESTITUTION_COEFFICIENT;
                }
                else if (circleDistanceY <= (wall.Height / 2)) {
                    // de bal botst met de muur in de x-as
                    double distanceWall = Math.Abs(movementSphere.OffsetX - wall.PositionX) - circleRadius - wall.Width / 2;
                    double bounceBack = Math.Abs(circlePositionX - movementSphere.OffsetX) - distanceWall;
                    int relativePosition = movementSphere.OffsetX > wall.PositionX ? 1 : -1;
                    offsetX = relativePosition * (bounceBack - distanceWall) * RESTITUTION_COEFFICIENT;
                    this.speedX *= -1 * RESTITUTION_COEFFICIENT;
                }
                else {
                    double cornerDistance_sq = Math.Pow((circleDistanceX - wall.Width / 2), 2) + Math.Pow((circleDistanceY - wall.Height / 2), 2);
                    if (cornerDistance_sq <= Math.Pow(circleRadius, 2)) {
                        // de bal botst met een hoek van de muur
                        // De hoek van de botsing bepalen
                        double cornerX = Math.Abs(wall.X1 - movementSphere.OffsetX) < Math.Abs(wall.X2 - movementSphere.OffsetX) ? wall.X1 : wall.X2;
                        double cornerY = Math.Abs(wall.Y1 - movementSphere.OffsetY) < Math.Abs(wall.Y2 - movementSphere.OffsetY) ? wall.Y1 : wall.Y2;

                        double x = 0.0;
                        double y = 0.0;

                        // De positie van de bal bepalen bij de botsing
                        // Als er geen verplaatsing is in x wordt er met de formule x = a gewerkt, anders met y = mx + b
                        if (offsetX == 0) {
                            x = movementSphere.OffsetX;

                            double z = Math.Sqrt(-Math.Pow(cornerX, 2) + 2 * cornerX * x + Math.Pow(circleRadius, 2) - Math.Pow(x, 2));
                            double y1 = cornerY - z;
                            double y2 = cornerY + z;

                            y = Math.Abs(y1 - movementSphere.OffsetY) < Math.Abs(y2 - movementSphere.OffsetY) ? y1 : y2;
                        }
                        else {
                            double m = (circlePositionY - movementSphere.OffsetY) / (circlePositionX - movementSphere.OffsetX);
                            double b = movementSphere.OffsetY - (m * movementSphere.OffsetX);

                            double qa = Math.Pow(m, 2) + 1;
                            double qb = 2 * (m * b - m * cornerY - cornerX);
                            double qc = Math.Pow(cornerY, 2) - Math.Pow(circleRadius, 2) + Math.Pow(cornerX, 2) - 2 * b * cornerY + Math.Pow(b, 2);

                            double d = Math.Pow(qb, 2) - 4 * qa * qc;

                            if (d > 0) {
                                // als de discriminant groter is dan 0 zijn er 2 oplossingen, de oplossing die het dichtste bij de positie van de bal ligt is de juiste
                                double x1 = (-qb + Math.Sqrt(d)) / (2 * qa);
                                double x2 = (-qb - Math.Sqrt(d)) / (2 * qa);

                                x = Math.Abs(x1 - movementSphere.OffsetX) < Math.Abs(x2 - movementSphere.OffsetX) ? x1 : x2;

                                y = m * x + b;
                            }
                            else {
                                // Als de discriminant gelijk is aan nul is de lijn tangeniaal met de cirkel en is er dus geen botsing
                                continue;
                            }
                        }

                        double limitedTimeframe = 0;

                        // De lengte van de periode na de botsing berekenen, de conditionele statement voorkomt een deling door nul
                        // aangezien er niet naar de acceleratie wordt gekeken is dit een ruwe benadering
                        if (speedX != 0) {
                            limitedTimeframe = TIMEFRAME - (x - movementSphere.OffsetX) / speedX;
                        }
                        else if (speedY != 0) {
                            limitedTimeframe = TIMEFRAME - (y - movementSphere.OffsetY) / speedY;
                        }

                        double xVector = x - cornerX;
                        double yVector = y - cornerY;

                        // Aan de hand van de vector tussen de hoek en het botspunt kan de vector van de terugbots berekend worden
                        double c = -2 * (speedX * xVector + speedY * yVector) / (xVector * xVector + yVector * yVector);
                        speedX = (speedX + c * xVector);
                        speedY = (speedY + c * yVector);

                        // Aan de hand van de vector tussen hoek en botspunt de resitutie coëfficient bepalen
                        speedX -= speedX * ((1 - RESTITUTION_COEFFICIENT) * (Math.Abs(xVector) / circleRadius) );
                        speedY -= speedY * ((1 - RESTITUTION_COEFFICIENT) * (Math.Abs(yVector) / circleRadius));

                        // de offset wordt aangepast naar de juiste waarden
                        offsetX = x + limitedTimeframe * speedX * CONVERSION_RATE - movementSphere.OffsetX;
                        offsetY = y + limitedTimeframe * speedY * CONVERSION_RATE - movementSphere.OffsetY;
                    }
                }
            }
        }

        /// <summary>
        /// Muur toevoegen aan het bordspel
        /// </summary>
        /// <param name="x1">int: 1st coördinate</param>
        /// <param name="x2">int: 2nd coördinate</param>
        /// <param name="y1">int: 3rd coördinate</param>
        /// <param name="y2">int: 4th coördinate</param>
        /// <returns></returns>
        private GeometryModel3D AddWall(int x1, int x2, int y1, int y2) {
            Wall wall = new Wall(x1, x2, y1, y2);
            this.walls.Add(wall);
            GeometryModel3D model = wall.WallModel;
            MaterialGroup materialGroup = new MaterialGroup();
            materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("Red"))));
            materialGroup.Children.Add(new SpecularMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#CCCCCC")), 50));
            model.Material = materialGroup;
            return model;
        }
    }
}
